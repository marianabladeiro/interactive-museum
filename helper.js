"use strict";

//  Adapted from Daniel Rohmer tutorial
//
// 		https://imagecomputing.net/damien.rohmer/teaching/2019_2020/semester_1/MPRI_2-39/practice/threejs/content/000_threejs_tutorial/index.html
//
// 		J. Madeira - April 2021

const helper = {

    initEmptyScene: function (sceneElements) {

        // ************************** //
        // Create the 3D scene
        // ************************** //
        sceneElements.sceneGraph = new THREE.Scene();
        sceneElements.sceneGraph.translateX(3).translateY(-0.09).translateZ(-2.5);

        // ************************** //
        // Add camera
        // ************************** //
        const width = window.innerWidth;
        const height = window.innerHeight;
        const camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 500);
        sceneElements.camera = camera;
        camera.position.set(-0.1, 1, 6);
        camera.lookAt(0, 0, 0);

        // ************************** //
        // NEW --- Control for the camera
        // ************************** //
        sceneElements.control = new THREE.OrbitControls(camera);
        sceneElements.control.screenSpacePanning = true;
        sceneElements.control.target.set(0,1,0);
        camera.lookAt(sceneElements.control.target);

        // ************************** //
        // Illumination
        // ************************** //

        // ************************** //
        // Add ambient light
        // ************************** //
        const ambientLight = new THREE.AmbientLight('rgb(255, 255, 255)', 0.2);
        sceneElements.sceneGraph.add(ambientLight);

        // ***************************** //
        // Add spotlight (with shadows)
        // ***************************** //
        const spotLight = new THREE.SpotLight('rgb(255, 255, 255)', 0.8);
        spotLight.position.set(-5, 8, 0);
        sceneElements.sceneGraph.add(spotLight);

        // Setup shadow properties for the spotlight
        spotLight.castShadow = true;
        spotLight.shadow.mapSize.width = 2048;
        spotLight.shadow.mapSize.height = 2048;
        spotLight.translateX(-4.5).translateZ(3);

        // Give a name to the spot light
        spotLight.name = "light";

        //spaceship
        const light1 = new THREE.SpotLight(0xab20fd, 2.3, 2.5);
        light1.translateX(3).translateY(1.8).translateZ(-4);
        sceneElements.sceneGraph.add(light1);
        light1.castShadow = true;

        light1.name = "light1";

        const light12 = new THREE.PointLight(0xab20fd, 2.3, 2);
         light12.translateX(4).translateY(1.5).translateZ(-4);
         sceneElements.sceneGraph.add(light12);
         light12.castShadow = true;
         light12.name = "light12";

        const light2 = new THREE.PointLight(0x005eff, 2.3, 2.5);
        light2.translateX(2.5).translateY(1.8).translateZ(-1.5);
        sceneElements.sceneGraph.add(light2);
        light2.castShadow = true;

         light2.name = "light2";

         const light13 = new THREE.PointLight(0x005eff, 2.3, 2.5);
        light13.translateX(4).translateY(1.8).translateZ(-0.8);
        sceneElements.sceneGraph.add(light13);
        light13.castShadow = true;

        const light3 = new THREE.PointLight(0xe0aa3e, 2, 2.4);
        light3.translateX(1.6).translateY(1.3).translateZ(-2.6);
        sceneElements.sceneGraph.add(light3);
        light3.castShadow = true;
         light3.name = "light3";

         const light4 = new THREE.PointLight(0xe0aa3e, 2.0, 2.2);
        light4.translateX(3.5).translateY(1.3).translateZ(-3);
        //sceneElements.sceneGraph.add(light4);
        light4.castShadow = true;
         light4.name = "light4";

         const light5 = new THREE.PointLight(0xe0aa3e, 1.5, 2);
        light5.translateX(3).translateY(0.5).translateZ(-2.8);
        //sceneElements.sceneGraph.add(light5);
        light5.castShadow = true;
         light5.name = "light5";

         //for dome
         const light7 = new THREE.PointLight(0xffffff, 1.5, 2);
         light7.translateX(1.8).translateY(1.5).translateZ(4.5);
         sceneElements.sceneGraph.add(light7);
         light7.castShadow = true;
         light7.name = "light7";

          const light8 = new THREE.PointLight(0xffffff, 1.5, 2);
         light8.translateX(-1).translateY(1.5).translateZ(4.5);
         sceneElements.sceneGraph.add(light8);
         light8.castShadow = true;
         light8.name = "light8";

         const light9 = new THREE.PointLight(0xffffff, 1.5, 2);
         light9.translateX(-2).translateY(1.5).translateZ(3);
         sceneElements.sceneGraph.add(light9);
         light9.castShadow = true;
         light9.name = "light9";

         const light10 = new THREE.PointLight(0xffffff, 1.5, 2);
         light10.translateX(0.4).translateY(0.5).translateZ(3.4);
         sceneElements.sceneGraph.add(light10);
         light10.castShadow = true;
         light10.name = "light10";

         //greek pillar
         const light11 = new THREE.PointLight(0xffffff, 1, 2);
         light11.translateX(-2.6).translateY(2.5).translateZ(-2.5);
         sceneElements.sceneGraph.add(light11);
         light11.castShadow = true;
         light11.name = "light11";

         //for red cube
         const light14 = new THREE.PointLight(0xffffff, 1, 4);
         light14.translateX(3.2).translateY(2).translateZ(1.3);
         sceneElements.sceneGraph.add(light14);
         light14.castShadow = true;
         light14.name = "light14";

         let red = 1;

         var changeColor = function ()
          {
            if(red == 1)
              {
                renderer.setClearColor('rgb(255, 219, 222)', 1.0);
                red = 0;
              }
              else if(red == 0)
              {
                renderer.setClearColor('rgb(255, 255, 255)', 1.0);
                red = 1;
              }
          }


        // *********************************** //
        // Create renderer (with shadow map)
        // *********************************** //
        const renderer = new THREE.WebGLRenderer({ antialias: true });
        sceneElements.renderer = renderer;
        renderer.name = "renderer";
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setClearColor('rgb(255, 219, 222)', 1.0);
        renderer.setSize(width, height);

        // Setup shadowMap property
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap;


        // **************************************** //
        // Add the rendered image in the HTML DOM
        // **************************************** //
        const htmlElement = document.querySelector("#Tag3DScene");
        htmlElement.appendChild(renderer.domElement);
    },

    render: function render(sceneElements) {
        sceneElements.renderer.render(sceneElements.sceneGraph, sceneElements.camera);
    },
};